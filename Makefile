GO = go
export DB_HOST=localhost
export DB_NAME=postgres
export DB_USER=postgres
export DB_PORT=5432
export DB_PASSWORD=123

all: docker_compose

build:
	$(GO) build cmd/main.go
	./main

docker_compose:
	sudo docker-compose up --build main

test:
	go test -v ./tests

delete:
	rm main