package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"testing"
)

func TestExecuteCommand200(t *testing.T) {
	expectedResponse := "Hello, World!\n"

	url := "http://localhost:8080/execute_command/1"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "admin_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Expected status code 200, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != expectedResponse {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", expectedResponse, string(body))
	}
	fmt.Println("TestExecuteCommand200 passed successfully!")
}

func TestExecuteCommand404(t *testing.T) {
	url := "http://localhost:8080/execute_command"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "user_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected status code 404, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != "404 page not found\n" {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", "404 page not found\n", string(body))
	}
	fmt.Println("TestExecuteCommand404 passed successfully!")
}

func TestExecuteCommand401(t *testing.T) {
	url := "http://localhost:8080/execute_command/1"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusUnauthorized {
		t.Fatalf("Expected status code 401, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != "" {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", "404 page not found\n", string(body))
	}
	fmt.Println("TestExecuteCommand401 passed successfully!")
}

func TestExecuteStopCommand400(t *testing.T) {
	url := "http://localhost:8080/execute_command/2"

	client := &http.Client{}

	req, err := http.NewRequest("PATCH", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "admin_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected status code 400, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != "" {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", "404 page not found\n", string(body))
	}
	fmt.Println("TestExecuteStopCommand400 passed successfully!")
}

func TestGetCommand200(t *testing.T) {
	expectedResponse := "#!/bin/bash\nls"

	url := "http://localhost:8080/get_command/2"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "admin_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Expected status code 200, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != expectedResponse {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", expectedResponse, string(body))
	}
	fmt.Println("TestGetCommand200 passed successfully!")
}

func TestGetCommand404(t *testing.T) {
	url := "http://localhost:8080/get_command/5"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "admin_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected status code 404, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != "" {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", "\n", string(body))
	}
	fmt.Println("TestGetCommand404 passed successfully!")
}

func TestGetCommandList200(t *testing.T) {
	expectedRespones := "1. #!/bin/bash\necho Hello, World!\n2. #!/bin/bash\nls\n3. #!/bin/bash\necho Hello\nsleep 5\necho bye\n4. #!/bin/bash\necho 1\nsleep 1\necho 2\nsleep 1\necho 3\n"

	url := "http://localhost:8080/get_command_list"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "admin_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Expected status code 200, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != expectedRespones {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", expectedRespones, string(body))
	}
	fmt.Println("TestGetCommandList200 passed successfully!")
}

func TestCreateCommand201(t *testing.T) {
	url := "http://localhost:8080/new_command"

	client := &http.Client{}

	commandBody := bytes.NewBufferString("#!/bin/bash\n ls -a")

	req, err := http.NewRequest("POST", url, commandBody)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "admin_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		t.Fatalf("Expected status code 201, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != "" {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", "\n", string(body))
	}
	fmt.Println("TestCreateCommand201 passed successfully!")
}

func TestCreateCommand403(t *testing.T) {
	url := "http://localhost:8080/new_command"

	client := &http.Client{}

	commandBody := bytes.NewBufferString("#!/bin/bash\n ls -a")

	req, err := http.NewRequest("POST", url, commandBody)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	req.Header.Set("token", "user_token")

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusForbidden {
		t.Fatalf("Expected status code 403, but got %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body) != "" {
		t.Fatalf("Response does not match expected answer\nExpected: %s\nActual: %s", "\n", string(body))
	}
	fmt.Println("TestCreateCommand403 passed successfully!")
}
