package storage

import (
	"fmt"
	"os"
)

var PsqlInfo = fmt.Sprintf("host=%s port=%s user=%s "+
	"password=%s dbname=%s sslmode=disable",
	Host, Port, User, Password, Dbname)

var (
	Host     = os.Getenv("DB_HOST")
	User     = os.Getenv("DB_USER")
	Password = os.Getenv("DB_PASSWORD")
	Dbname   = os.Getenv("DB_NAME")
	Port     = os.Getenv("DB_PORT")
)

type Command struct {
	ID      int
	Command string
}
