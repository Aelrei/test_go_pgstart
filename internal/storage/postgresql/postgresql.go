package postgresql

import (
	"bufio"
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/Aelrei/test_go_pgstart/internal/getters"
	"gitlab.com/Aelrei/test_go_pgstart/internal/storage"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

type Storage struct {
	db *sql.DB
}

func New() (*Storage, error) {
	db, err := sql.Open("postgres", storage.PsqlInfo)
	if err != nil {
		return nil, fmt.Errorf("%s", err)
	}

	gen, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS commands (
			id SERIAL PRIMARY KEY,
			command TEXT,
			active BOOLEAN,
			output_log TEXT  
	);`)

	if err != nil {
		return nil, fmt.Errorf("%s: %w", gen, err)
	}

	return &Storage{db: db}, nil

}

func CheckPostgresDB(db *sql.DB) error {
	query := `
		SELECT COUNT(*) FROM information_schema.tables
		WHERE table_name IN ('commands');
	`
	var count int
	err := db.QueryRow(query).Scan(&count)
	if err != nil {
		return err
	}

	if count == 1 {
		log.Println("database exists")
	} else {
		store, err := New()
		if err != nil {
			log.Fatalln("failed to init storage", err)
		} else {
			log.Println("success init storage")
		}

		store, err = UpdateStorage()
		if err != nil {
			log.Fatalln("failed to update storage")
		} else {
			log.Println("success update storage")
		}
		_ = store
	}
	return nil
}

func UpdateStorage() (*Storage, error) {
	db, err := sql.Open("postgres", storage.PsqlInfo)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}
	defer db.Close()

	query := `
        INSERT INTO commands (command, active) VALUES ('#!/bin/bash
echo Hello, World!', false);
        INSERT INTO commands (command, active) VALUES ('#!/bin/bash
ls', false);
	    INSERT INTO commands (command, active) VALUES ('#!/bin/bash
echo Hello
sleep 5
echo bye', false);
		INSERT INTO commands (command, active) VALUES ('#!/bin/bash
echo 1
sleep 1
echo 2
sleep 1
echo 3', false);

    `

	_, err = db.Exec(query)
	if err != nil {
		return nil, fmt.Errorf("error during request: %w", err)
	}

	return &Storage{db: db}, nil
}

func InsertCommand(db *sql.DB, command string) error {
	scanner := bufio.NewScanner(strings.NewReader(command))
	scanner.Scan()
	firstLine := scanner.Text()

	if firstLine != "#!/bin/bash" {
		return errors.New("command does not start with '#!/bin/bash'")
	}

	maxId, _ := getters.GetMaxBannerIdFromDB(db)
	_, err := db.Exec("INSERT INTO commands (id, command, active) VALUES ($1, $2, $3)", maxId+1, command, false)
	if err != nil {
		return err
	}
	return nil
}

func ExecuteCommandByID(db *sql.DB, id int, cancel chan struct{}) ([]byte, error) {
	tx, err := db.Begin()
	if err != nil {
		return nil, fmt.Errorf("error starting transaction: %w", err)
	}
	defer tx.Rollback()

	_, err = tx.Exec("UPDATE commands SET active = true WHERE id = $1", id)
	if err != nil {
		return nil, fmt.Errorf("error setting command as active: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		return nil, fmt.Errorf("error committing transaction: %w", err)
	}

	var command string
	err = db.QueryRow("SELECT command FROM commands WHERE id = $1", id).Scan(&command)
	if err != nil {
		return nil, fmt.Errorf("error getting command from database: %w", err)
	}

	cmd := exec.Command("bash", "-c", command)

	var outputBuffer bytes.Buffer

	cmd.Stdout = io.MultiWriter(&outputBuffer, os.Stdout)
	cmd.Stderr = os.Stderr

	err = cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("error starting command: %w", err)
	}

	done := make(chan error)
	go func() {
		done <- cmd.Wait()
	}()

	go func() {
		for {
			select {
			case <-cancel:
				_, err := InsertIntermediateResult(db, id, outputBuffer.String())
				if err != nil {
					log.Printf("error inserting intermediate result into database: %v\n", err)
				}
				break
			default:
				_, err := InsertIntermediateResult(db, id, outputBuffer.String())
				if err != nil {
					log.Printf("error inserting intermediate result into database: %v\n", err)
				}
				time.Sleep(100 * time.Millisecond)
			}
		}
	}()

	select {
	case <-cancel:
		err := cmd.Process.Kill()
		if err != nil {
			return nil, fmt.Errorf("error killing command process: %w", err)
		}
	case err := <-done:
		if err != nil {
			return nil, fmt.Errorf("error executing command: %w", err)
		}
	}
	_, err = InsertInDB(db, nil, id, outputBuffer)
	if err != nil {
		return nil, fmt.Errorf("error inserting final result into database: %w", err)
	}

	return outputBuffer.Bytes(), nil
}

func InsertIntermediateResult(db *sql.DB, id int, output string) (int64, error) {
	result, err := db.Exec("UPDATE commands SET output_log = $2 WHERE id = $1", id, output)
	if err != nil {
		return 0, fmt.Errorf("error updating command output log in database: %w", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("error getting rows affected: %w", err)
	}
	return rowsAffected, nil
}

func InsertInDB(db *sql.DB, err error, id int, outputBuffer bytes.Buffer) ([]byte, error) {
	if err != nil {
		log.Printf("error killing command process: %v\n", err)
	}
	_, err = db.Exec("UPDATE commands SET active = false, output_log = $2 WHERE id = $1", id, outputBuffer.String())
	if err != nil {
		return nil, fmt.Errorf("error updating command status and output log in database: %w", err)
	}
	return outputBuffer.Bytes(), nil
}

func StopCommandByID(db *sql.DB, id int) error {
	_, err := db.Exec("UPDATE commands SET active = false WHERE id = $1", id)
	if err != nil {
		return err
	}
	return nil
}
