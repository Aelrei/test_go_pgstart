package getters

import (
	"database/sql"
	"fmt"
	"gitlab.com/Aelrei/test_go_pgstart/internal/storage"
)

func GetCommandsList(db *sql.DB) ([]storage.Command, error) {
	rows, err := db.Query("SELECT id, command FROM commands ORDER BY id")
	if err != nil {
		return nil, fmt.Errorf("failed to execute query: %w", err)
	}
	defer rows.Close()

	var commands []storage.Command

	for rows.Next() {
		var cmd storage.Command
		if err := rows.Scan(&cmd.ID, &cmd.Command); err != nil {
			return nil, fmt.Errorf("failed to scan row: %w", err)
		}
		commands = append(commands, cmd)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error after iterating rows: %w", err)
	}

	return commands, nil
}

func GetCommand(db *sql.DB, id string) (storage.Command, error) {
	var cmd storage.Command
	rows, err := db.Query("SELECT id, command FROM commands WHERE id = $1", id)
	if err != nil {
		return cmd, fmt.Errorf("failed to execute query: %w", err)
	}
	defer rows.Close()

	if rows.Next() {
		err := rows.Scan(&cmd.ID, &cmd.Command)
		if err != nil {
			return cmd, fmt.Errorf("failed to scan row: %w", err)
		}
		return cmd, nil
	}
	return cmd, sql.ErrNoRows
}

func GetMaxBannerIdFromDB(db *sql.DB) (int, error) {
	var maxId int
	query := "SELECT MAX(id) FROM commands"
	err := db.QueryRow(query).Scan(&maxId)
	if err != nil {
		return 0, fmt.Errorf("error getting max Id from database: %v", err)
	}
	return maxId, nil
}
