package handlers

import (
	"database/sql"
	"fmt"
	"gitlab.com/Aelrei/test_go_pgstart/internal/getters"
	"gitlab.com/Aelrei/test_go_pgstart/internal/http-server/accessHTTP"
	"gitlab.com/Aelrei/test_go_pgstart/internal/storage/postgresql"
	"io"
	"net/http"
	"strconv"
	"sync"
)

type Handle struct {
	db *sql.DB
}

var (
	cancelChannels map[string]chan struct{}
	isGETActive    bool
	mu             sync.Mutex
)

func init() {
	cancelChannels = make(map[string]chan struct{})
}

func New(db *sql.DB) Handle {
	return Handle{db: db}
}

func (H *Handle) HandlerNewCommand(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		body, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = postgresql.InsertCommand(H.db, string(body))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (H *Handle) HandlerExecuteCommand(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		mu.Lock()
		isGETActive = true
		mu.Unlock()

		strID := r.PathValue("id")
		intID, err := strconv.Atoi(strID)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		maxID, err := getters.GetMaxBannerIdFromDB(H.db)
		if intID > maxID {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		cancelChannel := make(chan struct{})
		mu.Lock()
		cancelChannels[strID] = cancelChannel
		mu.Unlock()

		result, err := postgresql.ExecuteCommandByID(H.db, intID, cancelChannel)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		isGETActive = accessHTTP.SelectCaseExecuteCommand(w, cancelChannel, &mu, isGETActive, result)
	case "PATCH":
		mu.Lock()
		strID := r.PathValue("id")
		intID, err := strconv.Atoi(strID)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if !isGETActive {
			w.WriteHeader(http.StatusBadRequest)
			mu.Unlock()
			return
		}
		cancelChannel, ok := cancelChannels[strID]
		if ok {
			close(cancelChannel)
			delete(cancelChannels, strID)
		}
		mu.Unlock()

		err = postgresql.StopCommandByID(H.db, intID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (H *Handle) HandlerCommandList(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		commands, err := getters.GetCommandsList(H.db)
		if err != nil {
			http.Error(w, fmt.Sprintf("Failed to get commands list: %v", err), http.StatusInternalServerError)
			return
		}
		var commandList string
		for _, cmd := range commands {
			commandList += fmt.Sprintf("%d. %s\n", cmd.ID, cmd.Command)
		}
		w.Write([]byte(commandList))
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (H *Handle) HandlerGetCommand(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		strID := r.PathValue("id")
		intID, err := strconv.Atoi(strID)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		maxID, err := getters.GetMaxBannerIdFromDB(H.db)
		if intID > maxID {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		command, err := getters.GetCommand(H.db, strID)
		if err != nil {
			http.Error(w, fmt.Sprintf("Failed to get command: %v", err), http.StatusInternalServerError)
			return
		}
		w.Write([]byte(command.Command))
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
