package accessHTTP

import (
	"net/http"
	"strings"
	"sync"
)

func SelectCaseExecuteCommand(w http.ResponseWriter, cancelChannel chan struct{}, mu *sync.Mutex, isGETActive bool, result []byte) bool {
	select {
	case <-cancelChannel:
		w.WriteHeader(http.StatusConflict)
		mu.Lock()
		isGETActive = false
		w.Write(result)
		mu.Unlock()
		return isGETActive
	default:
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	}
	return false
}

func AuthMiddlewareUserAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := strings.Split(r.Header.Get("token"), " ")
		if len(token) != 1 || token[0] == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else if token[0] == "user_token" {
			next.ServeHTTP(w, r)
		} else if token[0] == "admin_token" {
			next.ServeHTTP(w, r)
		} else {
			w.WriteHeader(http.StatusForbidden)
			return
		}
	})
}

func AuthMiddlewareAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := strings.Split(r.Header.Get("token"), " ")
		if len(token) != 1 {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else if token[0] == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else if token[0] != "admin_token" {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}
