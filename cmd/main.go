package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/Aelrei/test_go_pgstart/internal/http-server/accessHTTP"
	"gitlab.com/Aelrei/test_go_pgstart/internal/http-server/handlers"
	"gitlab.com/Aelrei/test_go_pgstart/internal/storage"
	"gitlab.com/Aelrei/test_go_pgstart/internal/storage/postgresql"
	"log"
	"net/http"
)

func main() {
	db, err := sql.Open("postgres", storage.PsqlInfo)
	if err != nil {
		log.Println("Failed to connect to the database:", err)
		return
	}
	defer db.Close()

	err = postgresql.CheckPostgresDB(db)
	if err != nil {
		log.Println("Failed to connect to the database:", err)
		return
	}

	handle := handlers.New(db)
	router := http.NewServeMux()

	router.Handle("/new_command", accessHTTP.AuthMiddlewareAdmin(http.HandlerFunc(handle.HandlerNewCommand)))
	router.Handle("/execute_command/{id}", accessHTTP.AuthMiddlewareAdmin(http.HandlerFunc(handle.HandlerExecuteCommand)))
	router.Handle("/get_command_list", accessHTTP.AuthMiddlewareUserAdmin(http.HandlerFunc(handle.HandlerCommandList)))
	router.Handle("/get_command/{id}", accessHTTP.AuthMiddlewareUserAdmin(http.HandlerFunc(handle.HandlerGetCommand)))

	err = http.ListenAndServe("0.0.0.0:8080", router)
	if err != nil {
		fmt.Println("Error: ", err)
	}
}
